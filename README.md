# Neovim Configuration

## Getting started

### Pre-requisites -

1. Neovim
2. NodeJS
3. LazyGit (git client)
4. wl-clipboard (for wayload) or xclip (for x11)

### Setup -

Example for installing formatters -

1. Install `stylua` using `cargo install stylua`. Add
   `$HOME/.cargo/bin` to `$PATH` variable.
2. Install `prettier`, globally.
