local M = {}

-- Convenient window switcher based on window-picker plugin
M.window_swap = function()
	local picker = require("window-picker")
	local winid = picker.pick_window()
	if winid then
		local curid = vim.api.nvim_get_current_win()
		local cur_buffer = vim.api.nvim_win_get_buf(curid)
		local tgt_buffer = vim.api.nvim_win_get_buf(winid)
		local cur_cursor = vim.api.nvim_win_get_cursor(curid)
		local tgt_cursor = vim.api.nvim_win_get_cursor(winid)
		vim.api.nvim_win_set_buf(winid, cur_buffer)
		vim.api.nvim_win_set_buf(curid, tgt_buffer)
		vim.api.nvim_win_set_cursor(winid, cur_cursor)
		vim.api.nvim_win_set_cursor(curid, tgt_cursor)
		vim.api.nvim_set_current_win(winid)
	end
end

return M
