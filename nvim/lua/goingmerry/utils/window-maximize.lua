local M = {}

-- Maximize current window by closing others except neotree
M.window_maximize = function()
	local neotree_id = require("neo-tree.sources.manager").get_state("filesystem").winid
	local cur_win_id = vim.api.nvim_get_current_win()
	local win_ids = vim.api.nvim_list_wins()
	for _, i in ipairs(win_ids) do
		if i ~= neotree_id and i ~= cur_win_id then
			vim.api.nvim_win_close(i, false)
		end
	end
end

return M
