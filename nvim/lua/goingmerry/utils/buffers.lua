local nmap = require("goingmerry.utils.nmap").nmap

nmap("ph", "<cmd>sp<cr>")
nmap("pv", "<cmd>vs<cr>")
nmap("bs", "<cmd>w<cr>")
nmap("bS", "<cmd>wa<cr>")
