local nmap = function(key, fn, opts)
	opts = opts or {}
	vim.keymap.set("n", "<leader>" .. key, fn, opts)
end

return { nmap = nmap }
