local nmap_utils = require("goingmerry.utils.nmap")
local nmap = nmap_utils.nmap

function _G.ReloadConfig()
	local hls_status = vim.v.hlsearch
	for name, _ in pairs(package.loaded) do
		if name:match("^goingmerry") then
			package.loaded[name] = nil
		end
	end

	dofile(vim.env.MYVIMRC)
	if hls_status == 0 then
		vim.opt.hlsearch = false
	end
end

vim.cmd("command! ReloadConfig lua ReloadConfig()")

nmap("R", "<Cmd>lua ReloadConfig()<CR>")
