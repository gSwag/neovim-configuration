require("goingmerry.utils.loadpkg").loadpkg("harpoon", function()
	local nmap = require("goingmerry.utils.nmap").nmap

	nmap("mm", function()
		require("harpoon.mark").add_file()
	end)

	nmap("mn", function()
		require("harpoon.ui").nav_next()
	end)

	nmap("mp", function()
		require("harpoon.ui").nav_prev()
	end)

	nmap("m1", function()
		require("harpoon.ui").nav_file(1)
	end)

	nmap("m2", function()
		require("harpoon.ui").nav_file(2)
	end)

	nmap("m3", function()
		require("harpoon.ui").nav_file(3)
	end)

	nmap("m4", function()
		require("harpoon.ui").nav_file(4)
	end)

	nmap("mo", function()
		require("harpoon.ui").toggle_quick_menu()
	end)
end)
