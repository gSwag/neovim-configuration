local M = {}

-- Load a package named pkgname and
-- on successful load, call the callback with
-- the loaded package as argument
M.loadpkg = function(pkgname, callback)
	local status, pkg = pcall(require, pkgname)
	if not status then
		print("Could not load package:", pkgname)
		return
	end

	callback(pkg)
end

return M
