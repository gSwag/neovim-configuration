-- Default path for installation of lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

-- Git clone lazy if not already done
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

-- Add lazy installation path to lua path
vim.opt.rtp:prepend(lazypath)
