-- Global leader key
vim.g.mapleader = " "

-- Vim options/settings
vim.o.number = true
vim.o.scrolloff = 10
vim.o.hlsearch = false
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.termguicolors = true
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.o.autoindent = true

-- Diagnostic settings
vim.diagnostic.config({
	virtual_text = true,
	signs = true,
	update_in_insert = false,
	underline = true,
	severity_sort = true,
	float = true,
})

-- Additional file types
--
-- .templ file support
vim.filetype.add({
	extension = {
		templ = "templ",
	},
})

-- .templ file auto formatting with lsp
vim.api.nvim_create_autocmd({
	-- 'BufWritePre' event triggers just before a buffer is written to file.
	"BufWritePre",
}, {
	pattern = { "*.templ" },
	callback = function()
		-- Format the current buffer using Neovim's built-in LSP (Language Server Protocol).
		vim.lsp.buf.format()
	end,
})
