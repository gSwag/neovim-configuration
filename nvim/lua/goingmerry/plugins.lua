-- Load lazy
local status, lazy = pcall(require, "lazy")
if not status then
	print("Could not load lazy")
	return
end

-- Plugins
lazy.setup({
	-- Devicons
	"nvim-tree/nvim-web-devicons",

	-- Comfortable scrolloff
	{
		"Aasim-A/scrollEOF.nvim",
		event = "UIEnter",
		opts = {},
	},

	-- Clipboard provider
	{
		"matveyt/neoclip",
		config = function()
			vim.cmd([[set clipboard+=unnamedplus]])
		end,
	},

	-- Zen-mode
	{
		"folke/zen-mode.nvim",
		dependencies = {
			"folke/twilight.nvim",
		},
		opts = {
			window = {
				width = 0.8,
				options = {
					signcolumn = "yes",
					number = true,
				},
			},
			plugins = {
				twilight = { enabled = true },
				gitsigns = { enabled = false },
			},
		},
		keys = {
			{ "<leader>z", "<cmd>ZenMode<cr>", desc = "Toggle [Z]en Mode" },
		},
	},

	-- Lualine, nice modeline
	{
		"nvim-lualine/lualine.nvim",
		event = "UIEnter",
		opts = {
			options = {
				theme = "auto",
				-- Fix for disappearing dashboard -
				-- https://github.com/nvim-lualine/lualine.nvim/issues/773#issuecomment-1308901560
				-- after theme switch not needed anymore
				-- section_separators = "",
				-- component_separators = "",
			},
		},
	},

	-- Theme, vscode like theme
	{
		"EdenEast/nightfox.nvim",
		config = function()
			require("nightfox").setup({
				options = {
					styles = {
						comments = "italic",
						keywords = "bold",
						types = "italic,bold",
					},
				},
			})
			vim.cmd("colorscheme carbonfox")
		end,
	},

	-- UI Component library (others depend on it)
	"MunifTanjim/nui.nvim",

	-- Structured Search
	{
		"cshuaimin/ssr.nvim",
		opts = {
			border = "rounded",
			keymaps = {
				close = "q",
				next_match = "n",
				prev_match = "p",
				replace_confirm = "<cr>",
				replace_all = "<leader><cr>",
			},
		},
		keys = {
			{
				"<leader>fs",
				function()
					require("ssr").open()
				end,
				desc = "[S]tructured",
			},
		},
	},

	-- Nice dashboard
	{
		"glepnir/dashboard-nvim",
		event = "VimEnter",
		config = function()
			require("dashboard").setup({
				theme = "hyper",
				config = {
					week_header = {
						enable = true,
					},
					packages = { enable = true },
					project = { limit = 5 },
					mru = { limit = 5 },
				},
			})
		end,
	},

	-- Noice
	{
		"folke/noice.nvim",
		event = "VeryLazy",
		opts = {
			routes = {
				{
					filter = {
						event = "msg_show",
						kind = "",
						find = "written",
					},
					opts = { skip = true },
				},
				{
					filter = {
						event = "notify",
						min_height = 15,
					},
					opts = { skip = true },
				},
				{
					filter = {
						event = "msg_show",
						min_height = 15,
					},
					opts = { skip = true },
				},
			},
			lsp = {
				progress = { enabled = true },
			},
		},
		dependencies = {
			"rcarriga/nvim-notify",
		},
	},

	-- Multi-cursor
	"mg979/vim-visual-multi",

	-- Project manager
	{
		"ahmedkhalf/project.nvim",
		name = "project_nvim",
		opts = {},
	},

	-- Telescope, fuzzy finding.
	{
		"nvim-telescope/telescope.nvim",
		event = "VimEnter",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-telescope/telescope-fzy-native.nvim",
			"nvim-telescope/telescope-ui-select.nvim",
			"nvim-telescope/telescope-file-browser.nvim",
			"nvim-telescope/telescope-frecency.nvim",
			"kkharji/sqlite.lua",
			"ThePrimeagen/harpoon",
		},
		config = function()
			local telescope = require("telescope")
			telescope.setup({
				defaults = require("telescope.themes").get_ivy({
					layout_config = {
						height = 12,
					},
				}),
				extensions = {
					file_browser = {
						hijack_netrw = true,
					},
					fzy_native = {
						fuzzy = true,
						override_generic_sorter = true,
						override_file_sorter = true,
					},
					frecency = {
						show_scores = true,
						show_unindexed = true,
						use_sqlite = true,
						ignore_patterns = { "*.git/*", "node_modules/*" },
					},
				},
			})

			telescope.load_extension("file_browser")
			telescope.load_extension("fzy_native")
			telescope.load_extension("frecency")
			telescope.load_extension("harpoon")
		end,
		keys = {
			{
				"<leader>fp",
				function()
					require("telescope").extensions.projects.projects()
				end,
				desc = "[P]rojects",
			},
			{
				"<leader>ff",
				function()
					require("telescope.builtin").find_files()
				end,
				desc = "[F]iles",
			},
			{
				"<leader>fb",
				function()
					require("telescope.builtin").buffers()
				end,
				desc = "[B]buffers",
			},
			{
				"<leader>fF",
				function()
					require("telescope.builtin").git_files()
				end,
				desc = "Git [F]iles",
			},
			{
				"<leader>fg",
				function()
					require("telescope.builtin").live_grep()
				end,
				desc = "[G]rep",
			},
			{
				"<leader>fr",
				function()
					require("telescope.builtin").oldfiles()
				end,
				desc = "[R]ecent Files",
			},
			{
				"<leader>fh",
				function()
					require("telescope.builtin").help_tags()
				end,
				desc = "[H]elp",
			},
			{
				"<leader>fB",
				function()
					require("telescope").extensions.file_browser.file_browser()
				end,
				desc = "[B]rowser",
			},
			{
				"<leader>fm",
				function()
					require("telescope").extensions.harpoon.marks()
				end,
				desc = "[M]arks",
			},
			{
				"<leader>fr",
				function()
					require("telescope.builtin").registers()
				end,
				desc = "[R]egisters",
			},
			{
				"<leader>fj",
				function()
					require("telescope.builtin").jumplist()
				end,
				desc = "[J]umplist",
			},
			{
				"<leader>s",
				function()
					require("telescope.builtin").current_buffer_fuzzy_find()
				end,
				desc = "[S]earch in Buffer",
			},
		},
	},

	-- Terminal
	{
		"akinsho/toggleterm.nvim",
		opts = {
			open_mapping = "<C-t>",
			insert_mappings = true,
			direction = "float",
			shell = vim.o.shell,
			float_opts = {
				border = "curved",
			},
		},
		keys = {
			{ "<leader>ot", "<cmd>ToggleTerm<cr>", desc = "[T]erminal" },
			{
				"<leader>g",
				function()
					local terminal = require("toggleterm.terminal").Terminal:new({
						cmd = "lazygit",
						close_on_exit = true,
					})
					terminal:toggle()
				end,
				desc = "[G]it",
			},
		},
	},

	-- Hop easily
	{
		"phaazon/hop.nvim",
		opts = {},
		keys = {
			{
				"<leader>h",
				function()
					require("hop").hint_patterns()
				end,
				desc = "[H]op char",
			},
		},
	},

	-- Tree-sitter
	{
		"nvim-treesitter/nvim-treesitter",
		build = function()
			local ts_update = require("nvim-treesitter.install").update({
				with_sync = true,
			})
			ts_update()
		end,
		config = function()
			require("nvim-treesitter.configs").setup({
				auto_install = true,
				highlight = {
					enable = true,
				},
				rainbow = {
					enable = true,
					extended_mode = true,
					max_file_lines = nil,
				},
				autotag = {
					enable = true,
				},
			})
		end,
	},

	-- Rainbow brackets
	{
		"p00f/nvim-ts-rainbow",
		dependencies = "nvim-treesitter",
	},

	-- Auto pairs
	{
		"windwp/nvim-autopairs",
		event = { "InsertEnter", "BufEnter" },
		opts = {}, -- this is equalent to setup({}) function
	},

	-- Snippets
	{
		"L3MON4D3/LuaSnip",
		version = "2.*",
		build = "make install_jsregexp",
		dependencies = { "rafamadriz/friendly-snippets" },
		config = function()
			require("luasnip").setup({
				history = true,
			})
			require("luasnip.loaders.from_vscode").lazy_load()
		end,
	},

	-- Autocompletion
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lua",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			local cmp_autopairs = require("nvim-autopairs.completion.cmp")

			cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())

			-- Collected from https://nathan-long.com/blog/modern-javascript-tooling-in-neovim/
			-- It seems doing a better job than just selecting on Tab key
			local has_words_before = function()
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0
					and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				-- configure snippet support
				snippet = {
					expand = function(args)
						require("luasnip").lsp_expand(args.body)
					end,
				},

				mapping = {
					-- Tab to cycle through options
					-- ["<Tab>"] = cmp.mapping.select_next_item({
					-- 	behavior = cmp.SelectBehavior.Insert,
					-- }),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif luasnip.expand_or_jumpable() then
							luasnip.expand_or_jump()
						elseif has_words_before() then
							cmp.complete()
						else
							fallback()
						end
					end),

					["<S-Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_prev_item()
						elseif luasnip.jumpable(-1) then
							luasnip.jump(-1)
						else
							fallback()
						end
					end),

					-- `Enter` key to confirm completion
					["<CR>"] = cmp.mapping.confirm({ select = false }),
				},

				sources = {
					{ name = "nvim_lsp" },
					{ name = "luasnip" },
					{ name = "buffer" },
					{ name = "path" },
				},
			})
		end,
	},

	-- LSP
	{
		"VonHeikemen/lsp-zero.nvim",
		branch = "v2.x",
		event = "VimEnter",
		dependencies = {
			-- LSP support
			"neovim/nvim-lspconfig",
			"williamboman/mason-lspconfig.nvim",
			"williamboman/mason.nvim",
		},
		config = function()
			local lsp = require("lsp-zero").preset({})

			lsp.on_attach(function(_, bufnr)
				-- see :help lsp-zero-keybindings
				-- to learn the available actions
				lsp.default_keymaps({ buffer = bufnr })
			end)

			lsp.setup()

			-- lua lsp config
			require("lspconfig").lua_ls.setup(lsp.nvim_lua_ls())

			-- emmet setup
			require("lspconfig").emmet_language_server.setup({
				filetypes = {
					"css",
					"eruby",
					"html",
					"javascript",
					"javascriptreact",
					"less",
					"sass",
					"scss",
					"pug",
					"typescriptreact",
					"templ",
				},
			})
		end,

		keys = {
			{
				"<leader>lo",
				function()
					vim.lsp.buf.code_action({ context = { only = { "source.organizeImports" } } })
				end,
				desc = "[O]rganizse Imports",
			},
			{
				"<leader>lO",
				function()
					vim.lsp.buf.code_action({ context = { only = { "source.organizeImports.ts" } } })
				end,
				desc = "Typescript [O]rganizse Imports",
			},
			{ "<leader>lr", vim.lsp.buf.rename, desc = "[R]ename" },
			{
				"<leader>lR",
				function()
					require("telescope.builtin").lsp_references()
				end,
				desc = "[R]references",
			},
			{ "<leader>la", vim.lsp.buf.code_action, desc = "Code [A]ction" },
			{ "<leader>lh", vim.lsp.buf.hover, desc = "[H]over" },
			{
				"<leader>ld",
				function()
					require("telescope.builtin").lsp_definitions()
				end,
				desc = "[D]efinition",
			},
			{
				"<leader>lD",
				function()
					require("telescope.builtin").diagnostics()
				end,
				desc = "[D]iagnostics",
			},
			{
				"<leader>ls",
				function()
					require("telescope.builtin").lsp_document_symbols()
				end,
				desc = "Document [S]symbols",
			},
			{
				"<leader>lS",
				function()
					require("telescope.builtin").lsp_workspace_symbols()
				end,
				desc = "Workspace [S]symbols",
			},
			{
				"<leader>lwi",
				function()
					vim.cmd("LspInfo")
				end,
				desc = "[I]nfo",
			},
			{
				"<leader>lws",
				function()
					vim.cmd("LspStart")
				end,
				desc = "[S]tart",
			},
			{
				"<leader>lwS",
				function()
					vim.cmd("LspStop")
				end,
				desc = "[S]top",
			},
			{
				"<leader>lwr",
				function()
					vim.cmd("LspRestart")
				end,
				desc = "[R]estart",
			},
		},
	},

	-- Code formatting
	{
		"stevearc/conform.nvim",
		opts = {
			formatters_by_ft = {
				lua = { "stylua" },
				c = { "clang_format" },
				cpp = { "clang_format" },
				css = { "prettier" },
				scss = { "prettier" },
				python = { "black" },
				javascript = { "prettier" },
				javascriptreact = { "prettier" },
				typescript = { "prettier" },
				typescriptreact = { "prettier" },
				html = { "prettier" },
				json = { "prettier" },
				graphql = { "prettier" },
				go = { "golines", "gofumpt" },
				sh = { "shfmt" },
				sql = { "pg_format" },
				yaml = { "yamlfmt" },
				-- ["*"] = { "codespell" },
				["_"] = { "trim_whitespace" },
			},
			format_on_save = {
				lsp_fallback = true,
				timeout_ms = 1000,
			},
			log_level = vim.log.levels.WARN,
		},
	},

	-- Loading status indicator
	{
		"j-hui/fidget.nvim",
		tag = "legacy",
		opts = {},
	},

	-- Which-key
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 300
		end,
		config = function()
			local wk = require("which-key")
			wk.register({
				R = "[R]eload Config",
				w = { name = "[W]indow" },
				b = {
					name = "[B]buffer",
					s = "[S]ave Current",
					S = "[S]ave All",
				},
				f = { name = "[F]ind" },
				l = {
					name = "[L]sp",
					D = { name = "[D]iagnostics" },
					w = { name = "[W]orkspace" },
				},
				m = {
					name = "[M]arks",
					m = "[M]ark",
					n = "[N]ext",
					p = "[P]rev",
					["1"] = "Nav [1]",
					["2"] = "Nav [2]",
					["3"] = "Nav [3]",
					["4"] = "Nav [4]",
					o = "[O]pen Quick UI",
				},
				n = { name = "[N]eotree" },
				o = { name = "[O]pen" },
				p = {
					name = "S[p]lit",
					h = "[H]orizontally",
					v = "[V]ertically",
				},
				q = "[Q]uit",
			}, { prefix = "<leader>" })
		end,
	},

	-- Verticle file browser
	{
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v3.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		opts = {
			window = {
				width = 33,
				mappings = {
					["<Space>"] = false,
					["<Tab>"] = "open",
				},
			},
		},
		keys = {
			{ "<leader>no", "<cmd>Neotree reveal<cr>", desc = "[O]pen" },
			{ "<leader>nc", "<cmd>Neotree close<cr>", desc = "[C]lose" },
		},
	},

	-- Color picker and colorizer
	{
		"uga-rosa/ccc.nvim",
		opts = {
			highlighter = {
				auto_enable = true,
				lsp = true,
				auto_close = true,
				preserve = true,
			},
		},
		keys = {
			{ "<leader>oc", "<cmd>CccPick<cr>", desc = "[C]olor Picker" },
		},
	},

	-- Git signs
	{
		"lewis6991/gitsigns.nvim",
		event = "UIEnter",
		opts = {},
	},

	-- Git blame
	{
		"APZelos/blamer.nvim",
		event = "VimEnter",
		config = function()
			vim.g.blamer_enabled = true
			vim.g.blamer_delay = 100
		end,
	},

	-- Easy commenting code
	"tpope/vim-commentary",
	"JoosepAlviste/nvim-ts-context-commentstring",

	-- Autoclose, rename tags
	"windwp/nvim-ts-autotag",

	-- Move things around easily
	"matze/vim-move",

	-- Undo tree
	{
		"jiaoshijie/undotree",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		opts = {
			float_diff = false,
		},
		keys = {
			{
				"<leader>u",
				function()
					require("undotree").toggle()
				end,
				desc = "Toggle [U]ndo Tree",
			},
		},
	},

	-- Window picker
	{
		"s1n7ax/nvim-window-picker",
		name = "window-picker",
		opts = {
			highlights = {
				statusline = {
					focused = {
						fg = "#ededed",
						bg = "#3757bd",
						bold = true,
					},
					unfocused = {
						fg = "#ededed",
						bg = "#3757bd",
						bold = true,
					},
				},
				winbar = {
					focused = {
						fg = "#ededed",
						bg = "#3757bd",
						bold = true,
					},
					unfocused = {
						fg = "#ededed",
						bg = "#3757bd",
						bold = true,
					},
				},
			},
		},
		keys = {
			{
				"<leader>ww",
				function()
					local winid = require("window-picker").pick_window()
					if winid then
						vim.api.nvim_set_current_win(winid)
					end
				end,
				desc = "Switch",
			},
			{
				"<leader>ws",
				function()
					require("goingmerry.utils.window-swap").window_swap()
				end,
				desc = "[S]wap",
			},
			{
				"<leader>wm",
				function()
					require("goingmerry.utils.window-maximize").window_maximize()
				end,
				desc = "[M]aximize",
			},
			{ "<leader>wc", "<cmd>q<cr>", desc = "[C]lose" },
		},
	},

	-- Buffer delete
	{
		"kazhala/close-buffers.nvim",
		keys = {
			{
				"<leader>bc",
				function()
					require("close_buffers").delete({ type = "this" })
				end,
				desc = "[C]lose",
			},
		},
	},

	-- Todo comments
	{
		"folke/todo-comments.nvim",
		event = "UIEnter",
		opts = {
			signs = true,
			highlight = {
				multiline = true,
			},
		},
		keys = {
			{ "<leader>ft", "<cmd>TodoTelescope<cr>", desc = "[T]odo" },
		},
	},
})
